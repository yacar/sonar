import hello
import json

print(hello.say_something_to_someone("Yo","Bob"))
print(hello.say_hello("Uncle Bob"))
print(hello.say_happy_new_year("Uncle Bob"))
print(hello.say_bip("Robot"))
projects = hello.user_projects()
for project in projects:
    print(project.to_json(indent=4))
