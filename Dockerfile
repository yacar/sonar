FROM python:3.12.3-bookworm
#ENV DEBIAN_FRONTEND=noninteractive
# RUN apt-get update && apt-get upgrade && apt-get install python3-pip
# RUN python3 -m pip install pylint nose2
COPY requirements.txt ./
RUN apt-get update && apt-get install -y wget apt-transport-https gnupg lsb-release
RUN python3 -m pip install --no-cache-dir -r requirements.txt
RUN wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | apt-key add -
RUN echo deb https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main | tee -a /etc/apt/sources.list.d/trivy.list
RUN apt-get update
RUN apt-get install -y trivy
