import gitlab

def user_projects():
    gl = gitlab.Gitlab()

    # list all the projects
    projects = gl.projects.list(page=1, per_page=10)
    return projects
