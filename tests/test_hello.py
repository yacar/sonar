import unittest
import sys
sys.path.append('./src/hello')

from hello_funs import say_something_to_someone, say_hello, say_happy_new_year

class HelloFunsTests(unittest.TestCase):

    def test_say_something_to_someone(self):
        self.assertEqual(say_something_to_someone("Yo", "Bob"), "Yo Bob")
        self.assertEqual(say_something_to_someone("", ""), " ")

    def test_say_hello(self):
        self.assertEqual(say_hello("Bob"), "hello Bob")
        self.assertEqual(say_hello(""), "hello ")

    def test_say_happy_new_year(self):
        self.assertEqual(say_happy_new_year("Bob"), "happy new year Bob")
        self.assertEqual(say_happy_new_year(""), "happy new year ")
